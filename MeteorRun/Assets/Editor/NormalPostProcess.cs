using UnityEditor;
using UnityEngine;

public class NormalPostProcess : AssetPostprocessor {
	private void OnPostprocessTexture(Texture2D texture) {
		if (assetPath.Contains("Normal")) {
			var textureImporter = assetImporter as TextureImporter;
			if (textureImporter) {
				textureImporter.textureType = TextureImporterType.NormalMap;
				AssetDatabase.Refresh();
			}
		}
	}
}