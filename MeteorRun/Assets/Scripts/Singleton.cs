using UnityEngine;

public abstract class Singleton<T> : MonoBehaviour where T : MonoBehaviour {
	[SerializeField] private bool nDestroyOnLoad = false;
	private static T instance;

	public static T Instance => instance;

	protected virtual void Awake() {
		if (instance && instance != this) {
			Destroy(gameObject);
		}
		else {
			instance = this as T;
			if(nDestroyOnLoad) DontDestroyOnLoad(gameObject);
		}
	}
}