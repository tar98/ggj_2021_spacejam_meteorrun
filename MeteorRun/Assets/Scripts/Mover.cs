using System.Collections;
using UnityEngine;

public class Mover : MonoBehaviour {
	[Header("Limiter")]
	[SerializeField] private float xMax = 25f;
    [SerializeField] private float xMin = -25f;
    [SerializeField] private float yMax = 11f;
    [SerializeField] private float yMin = -11f;

    [Header("Ship")]
    public GameObject ship = null;
    public float speed = 10;
    public float rotSpeed = 1;
    public Quaternion rotationGoal;

    private Rigidbody rigBody;
    private Coroutine coroutineRotation = null;

    private void Awake() {
	    rigBody = GetComponent<Rigidbody>();
    }

    private void Start() {
	    rigBody.WakeUp();
    }

    private void FixedUpdate()
    {
        float translation = Input.GetAxis("Vertical") * speed;
        float horizontal = Input.GetAxis("Horizontal") * speed;

        translation *= Time.deltaTime * GameManager.Instance.velocity;
        horizontal *= Time.deltaTime * GameManager.Instance.velocity;

        //transform.Translate(0, translation, 0);
        if (transform.position.x + horizontal > xMin && transform.position.x + horizontal < xMax)
        {
            transform.Translate(horizontal, 0, 0);
        }
        if (transform.position.y + translation > yMin && transform.position.y + translation < yMax)
        {
            transform.Translate(0, translation, 0);
        }

        if (Input.GetKeyDown(KeyCode.Space) && coroutineRotation == null)
        {
	        coroutineRotation = StartCoroutine(StartRotation());
        }
    }

    private IEnumerator StartRotation()
    {
        float x = ship.transform.rotation.x == 0 ? 180 : 0;
        rotationGoal = Quaternion.Euler(x, 180, 0);
        Quaternion rotGoal = rotationGoal;
        Quaternion startRot = ship.transform.rotation;
        float alpha = 0f;
        while (alpha < 1)
        {
            alpha += Time.deltaTime * rotSpeed;
            ship.transform.rotation = Quaternion.Lerp(startRot, rotGoal, alpha);
            yield return new WaitForEndOfFrame();
        }
        coroutineRotation = null;
    }

}
