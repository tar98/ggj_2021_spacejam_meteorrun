using UnityEngine;
using System;

public class Temperature : MonoBehaviour {
	[Header("Pivot")]
    public Transform colliderUp = null;
    public Transform colliderDown = null;

    [Header("Data")]
    public float distance = 5;
    [SerializeField, Range(-100, 100)]
    private float healthTop = 0;
    [SerializeField, Range(-100, 100)]
    private float healthBottom = 0;
	public event Action onDie = null;

    public float HealthTop {
	    get => healthTop; 
	    set {
		    healthTop = Mathf.Clamp(value, -100, 100);
		    GameManager.Instance.isPlayerAlive = IsDead(healthTop);
	    }
    }
    
    public float HealthBottom {
	    get => healthBottom;
	    set {
		    healthBottom = Mathf.Clamp(value, -100, 100);
		    GameManager.Instance.isPlayerAlive = IsDead(healthBottom);
	    }
    }

    // Update is called once per frame
    private void FixedUpdate() {
	    SideRaycast(colliderUp.up, distance, true);
	    SideRaycast(-colliderDown.up, distance, false);
    }

#if UNITY_EDITOR
	private void OnDrawGizmos() {
		RaycastHit hit;
		Ray ray = new Ray(transform.position, colliderUp.up);
		if (Physics.Raycast(ray, out hit, Mathf.Infinity)) {
			Gizmos.color = hit.distance < 5 ? Color.green : Color.red;
			Gizmos.DrawLine(ray.origin, hit.point);
		}
        
		ray = new Ray(transform.position, -colliderDown.up);
		if (Physics.Raycast(ray, out hit, Mathf.Infinity)) {
			Gizmos.color = hit.distance < 5 ? Color.green : Color.blue;
			Gizmos.DrawLine(ray.origin, hit.point);
		}
	}
#endif

	private void SideRaycast(Vector3 direction, float dist, bool isTop) {
		var ray = new Ray(transform.position, direction);
		
		if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity))
		{
			if (hit.distance < dist)
			{
				var damaged = hit.collider.GetComponent<IAlterationTemperature>();
				if (isTop) damaged?.DoAlterationTop();
				else damaged?.DoAlterationBottom();
			} 
		}
	}

	public bool IsDead(float health) {
        if (health <= -100.0f || health >= 100.0f)
        {
			onDie?.Invoke();
        }
		return !(health <= -100.0f || health >= 100.0f);
	}
}

public interface IAlterationTemperature {
    public void DoAlterationTop();
    public void DoAlterationBottom();
}
