﻿using System.Globalization;
using UnityEngine;

public class Borderline : MonoBehaviour, IAlterationTemperature
{
    Temperature temperature;
    public float value;

    private void Start() {
        temperature = GameManager.Instance.temperature;
    }

    public void DoAlterationTop()
    {
        temperature.HealthTop += this.value;
        //Debug.Log($"<color=red>temperature top: {temperature.HealthTop.ToString(CultureInfo.InvariantCulture)}</color>");
    }
    public void DoAlterationBottom()
    {
        temperature.HealthBottom += this.value;
        //Debug.Log($"<color=cyan>temperature bottom: {temperature.HealthBottom.ToString(CultureInfo.InvariantCulture)}</color>");
    }
}
