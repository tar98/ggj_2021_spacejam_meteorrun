using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManagement : MonoBehaviour {
    
	[Header("HUD")]
	[SerializeField] private Image coldBarTop;
	[SerializeField] private Image warmBarTop;
	[SerializeField] private Image coldBarBottom;
	[SerializeField] private Image warmBarBottom;
	
	[Header("Menu")]
	[SerializeField] private GameObject pausePanel;
    [SerializeField] private GameObject helpPanel;
    [SerializeField] private GameObject gameoverPanel;

    bool pausePanelOpen = false;
    bool helpPanelOpen = false;
    bool gameoverPanelOpen = false;

    public bool isUpdateBar = true;

    private Temperature temperature;
    private void Awake()
    {
        temperature = GetComponentInParent<Temperature>();
    }


    private void LateUpdate() {
	    ButtonInteraction();
        if (isUpdateBar)
        {
            UpdateBar();
        }
        
    }

    public void PauseGame()
    {
        if (!helpPanelOpen && !pausePanelOpen)
        {
	        GameManager.Instance.isPaused = true;
	        pausePanel.SetActive(true);
            pausePanelOpen = true;
        }
          
    }

    public void ResumeGame() {
	    GameManager.Instance.isPaused = false;
        pausePanel.SetActive(false);
        helpPanel.SetActive(false);

        pausePanelOpen = false;
        helpPanelOpen = false;
    }
    
    public void HelpMenu() {  
        if(!helpPanelOpen && !pausePanelOpen)
        {
	        GameManager.Instance.isPaused = true;
	        helpPanel.SetActive(true);
            helpPanelOpen = true;
        }
    }

    public void GameOver()
    {
        if (!helpPanelOpen && !pausePanelOpen)
        {
            GameManager.Instance.isPaused = true;
            gameoverPanel.SetActive(true);
            gameoverPanelOpen = true;
        }
    }

    public void RestartGame()
    {
        SceneManager.LoadScene("Spawn Test");
    }

    public void MainMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    private void ButtonInteraction() {
	    if (Input.GetKeyDown(KeyCode.P)) {
		    PauseGame();
	    }
	    else if (Input.GetKeyDown(KeyCode.H)) {
		    HelpMenu();
	    }
	    if (helpPanelOpen || pausePanel) {
		    if (Input.GetKeyDown(KeyCode.Escape)) {
			    ResumeGame();
		    }
	    }
    }

    private void UpdateBar()
    {
        
        if (!GameManager.Instance.isPaused)
        {
            if (temperature.HealthTop <= 0)
            {
                coldBarTop.SetFiller(Mathf.Abs(temperature.HealthTop), 100);
            }
            else if (temperature.HealthTop>0)
            {
                warmBarTop.SetFiller(Mathf.Abs(temperature.HealthTop), 100);
            }

            if (temperature.HealthBottom <= 0)
            {
                coldBarBottom.SetFiller(Mathf.Abs(temperature.HealthBottom), 100);
            }
            else if (temperature.HealthBottom > 0)
            {
                warmBarBottom.SetFiller(Mathf.Abs(temperature.HealthBottom), 100);
            }

            
            
        }
 
    }

 
}

public static class extension
{
  public static void SetFiller(this Image image, float value, float max)
    {
        image.fillAmount = value / max;
    }
}
