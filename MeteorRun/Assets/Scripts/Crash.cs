using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crash : MonoBehaviour
{
    public ParticleSystem ps = null;
    public UIManagement uiMan = null;
    public GameObject navetta = null;

    private void Start()
    {
        GameManager.Instance.temperature.onDie += Die;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Asteroide"))
        {

            Die();
        }
    }

    private void Die()
    {
        Debug.Log("Muoerte sorte!");
        ps.Play();
        uiMan.GameOver();
        navetta.gameObject.SetActive(false);
        GameManager.Instance.isPlayerAlive = false;
    }
}
