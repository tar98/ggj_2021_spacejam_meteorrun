using UnityEngine;

public class Parallax : MonoBehaviour {
    private float length, startpos;
    private new Transform camera;
    public float parallaxEffect;

    private void Start() {
        startpos = transform.position.x;
        length = GetComponent<SpriteRenderer>().bounds.size.x;
        camera = GameManager.Instance.main.transform;
    }

    private void LateUpdate() {
	    Vector3 pos = camera.position;
        float temp = (pos.x * (1 - parallaxEffect));
        float dist = (pos.x * parallaxEffect);

        camera.position = new Vector3(startpos + dist, transform.position.y, pos.z);

        if (temp > startpos + length) startpos += length;
        else if (temp < startpos - length) startpos -= length;
    }
}
