using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
	public void StartGame()
    {
        SceneManager.LoadScene("Spawn Test");
    }
    public void CreditsMenu()
    {
        SceneManager.LoadScene("Credits");
    }
    public void BackCredits()
    {
        
        SceneManager.LoadScene("Menu");
    }
    
    public void ExitGame()
    {
        Debug.Log("si");
        Application.Quit();
    }
}
