using Unity.Burst;
using Unity.Jobs;
using UnityEngine;
using UnityEngine.Jobs;

public class Pathway : MonoBehaviour {
	private NativeTransform native;
	private TransformAccessArray transformAccessArray;
	public bool useJob = false;

	private void Start() {
		transformAccessArray = new TransformAccessArray(1);
		transformAccessArray.Add(transform);
	}

	private void Update() {
		if (useJob) {
			native = new NativeTransform(transform.right, Time.deltaTime, GameManager.Instance.velocity);
			JobHandle handle = native.Schedule(transformAccessArray);
			handle.Complete();
		}
		else {
			transform.position -= transform.right * Time.deltaTime * GameManager.Instance.velocity;
		}
	}

	private void OnDestroy() {
		transformAccessArray.Dispose();
	}
}

[BurstCompile]
public struct NativeTransform : IJobParallelForTransform {
	private Vector3 direction;
	private float deltaTime;
	private float vel;

	public NativeTransform(Vector3 direction, float deltaTime, float vel) {
		this.direction = direction;
		this.deltaTime = deltaTime;
		this.vel = vel;
	}

	public void Execute(int index, TransformAccess transform) {
		transform.position -= direction * deltaTime * vel;
	}
}
