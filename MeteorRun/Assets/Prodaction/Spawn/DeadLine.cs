using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class DeadLine : MonoBehaviour {
	[SerializeField] private bool debugLog = false;
	private BoxCollider boxCollider;

	private void Awake() {
		boxCollider = GetComponent<BoxCollider>();
	}

	private void OnTriggerExit(Collider other) {
		if(debugLog) print($"GameObject <color=orange>{other.name}</color> Trigger!");
		
		var meteor = other.GetComponentInParent<Meteor>();
		
		if (meteor) {
			meteor.OnDie();
			return;
		}

		var back = other.GetComponentInChildren<Background>();
		
		if (back) {
			back.OnRelease();
		}
	}


#if UNITY_EDITOR
	private void OnDrawGizmos() {
		if (boxCollider != null) {
			Gizmos.color = Color.red;
			Gizmos.DrawCube(transform.position, boxCollider.size);
		}
		else {
			Gizmos.color = Color.red;
			Gizmos.DrawCube(transform.position, GetComponent<BoxCollider>().size);
		}
	}
#endif
	
}
