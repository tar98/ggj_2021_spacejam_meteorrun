using UnityEngine;

public class Background : MonoBehaviour {
	private BackgroundSpawner backgroundSpawner;

	private void Awake() {
		backgroundSpawner = GetComponentInParent<BackgroundSpawner>();
	}

	private void LateUpdate() {
		transform.position -= transform.right * Time.deltaTime * (GameManager.Instance.velocity / 2);
	}

	public void OnRelease() {
		backgroundSpawner.pool.Get();
		backgroundSpawner.pool.Release(this);
	}
}