using System;
using UnityEngine;
using UnityEngine.Assertions;
using Random = UnityEngine.Random;

public class Meteor : MonoBehaviour {
	[SerializeField] private MeteorData meteorData;
	private IAlterationTemperature alternation;
	private Temperature temperature;
	private Rigidbody rigBody;
	public event Action<Meteor> Die;

	private void Awake() {
		rigBody = GetComponentInChildren<Rigidbody>();
		Assert.IsNotNull(rigBody, $"Prefab {name} : Rigidbody not Found!");
		temperature = GameManager.Instance.temperature;
	}

	private void Start() {
		float x = Random.Range(10, 300);
		float y = Random.Range(10, 300);
		float z = Random.Range(10, 300);
		rigBody.AddTorque(x, y, z, ForceMode.Acceleration);

		alternation = meteorData;
	}

	private void LateUpdate() {

		if ( Vector3.Distance(transform.position, temperature.transform.position) < meteorData.distance) {
			float distA = Vector3.Distance(transform.position, temperature.colliderUp.transform.position);
			float distB = Vector3.Distance(transform.position, temperature.colliderDown.transform.position);
			
			if (distA < distB) alternation?.DoAlterationTop();
			else alternation?.DoAlterationBottom();
		}
	}

	private void OnDrawGizmos() {
		Gizmos.DrawWireSphere(transform.position, meteorData.distance);
	}

    private void OnTriggerEnter(Collider other)
    {
		if (other.CompareTag("Player"))
		{
			OnDie();
			GameManager.Instance.isPlayerAlive = false;
		}
	}
 //   private void OnCollisionEnter(Collision other) {
	//	if (other.collider.CompareTag("Player")) {
	//		OnDie();
	//		GameManager.Instance.isPlayerAlive = false;
	//	}
	//}

	public void OnDie() {
		Die?.Invoke(this);
	}
}
