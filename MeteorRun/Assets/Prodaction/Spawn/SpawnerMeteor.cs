using System.Collections;
using UnityEngine;
using UnityEngine.Pool;

public class SpawnerMeteor : BaseSpawner<Meteor> {
	[SerializeField] private Transform parent;
	private BoxCollider boxCollider;
	
	private void Awake() {
		if (parent == null) parent = transform;
		boxCollider = GetComponent<BoxCollider>();
	}

	protected override void Start() {
		pool = new ObjectPool<Meteor>(Create, Get, Release, Destroy, false, defaultCapacity, maxSize);
		StartCoroutine(Spawn());
	}
	
#if UNITY_EDITOR
	private void OnDrawGizmos() {
		if (boxCollider != null) {
			Gizmos.color = Color.green;
			Gizmos.DrawCube(transform.position, boxCollider.size);
		}
		else {
			Gizmos.color = Color.green;
			Gizmos.DrawCube(transform.position, GetComponent<BoxCollider>().size);
		}
	}
#endif

	#region OVERRIDE

	protected override Meteor Create() {
		Meteor meteor = Instantiate(RandomPrefab, GetPositionInBound(), Quaternion.identity, parent);
		meteor.Die += pool.Release;
		return meteor;
	}

	protected override void Release(Meteor pooled) {
		pooled.transform.position = GetPositionInBound();
		base.Release(pooled);
	}

	protected override void Destroy(Meteor pooled) {
		pooled.Die -= pool.Release;
		base.Destroy(pooled);
	}

	#endregion

	private Vector3 GetPositionInBound() {
		Bounds bounds = boxCollider.bounds;
		return new Vector3(
			Random.Range(bounds.min.x, bounds.max.x),
			Random.Range(bounds.min.y, bounds.max.y),
			Random.Range(bounds.min.z, bounds.max.z)
		);
	}
	

	private IEnumerator Spawn() {
		yield return new WaitForEndOfFrame();
		
		while (GameManager.Instance.isPlayerAlive) {
			pool.Get();
			yield return new WaitForSeconds(GameManager.Instance.timeOfSpawn);
		}
		
		pool.Dispose();
		pool.Clear();
	}
}