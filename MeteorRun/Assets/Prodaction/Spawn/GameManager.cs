using System;
using System.Collections;
using UnityEngine;

public class GameManager : Singleton<GameManager> {
	[Header("Game")]
	public float velocity = 3;
	public float timeOfSpawn = 3f;
	public float thresholdOfVelocity = 5f;
	public bool isPaused = false;
	public bool isPlayerAlive = true;
	private float currentVel;

	[NonSerialized] public Temperature temperature;
	[NonSerialized] public Camera main;

	protected override void Awake() {
		base.Awake();
		temperature = FindObjectOfType<Temperature>();
		main = GetComponent<Camera>();
	}

	private void Start() {
#if !(DEVELOPMENT_BUILD || UNITY_EDITOR)
        Debug.unityLogger.logEnabled = false; 
#endif
		currentVel = velocity;
		StartCoroutine(SpeedUp());
	}

	private void Update() {
		velocity = isPaused ? 0 : currentVel;
	}

	private IEnumerator SpeedUp() {
		while (isPlayerAlive) {
			yield return new WaitForSeconds(thresholdOfVelocity);
			velocity += Time.deltaTime;
			timeOfSpawn -= Time.deltaTime;
		}
	}
}