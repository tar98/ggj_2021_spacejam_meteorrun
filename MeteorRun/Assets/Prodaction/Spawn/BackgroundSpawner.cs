using UnityEngine;
using UnityEngine.Pool;

public class BackgroundSpawner : BaseSpawner<Background> {
	[SerializeField] private Transform spawn;
	
	protected override void Start() {
		pool = new ObjectPool<Background>(Create, Get, Release, Destroy, false, defaultCapacity, maxSize);
	}

	protected override Background Create() {
		return Instantiate(RandomPrefab, spawn.position, Quaternion.identity, transform);
	}
}