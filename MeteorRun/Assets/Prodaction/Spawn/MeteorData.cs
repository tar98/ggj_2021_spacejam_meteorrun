using System.Globalization;
using UnityEngine;

[CreateAssetMenu(fileName = "Meteor-", menuName = "Meteor Data")]
public class MeteorData : ScriptableObject, IAlterationTemperature {
	[Header("Data")]
	[Min(3f)] public float distance;
	[SerializeField] private float value = 0;
	
	public void DoAlterationTop() {
		GameManager.Instance.temperature.HealthTop += value;
		//Debug.Log($"<color=red>temperature top: {GameManager.Instance.temperature.HealthTop.ToString(CultureInfo.InvariantCulture)}</color>");
	}

	public void DoAlterationBottom() {
		GameManager.Instance.temperature.HealthBottom += value;
		//Debug.Log($"<color=red>temperature bottom: {GameManager.Instance.temperature.HealthBottom.ToString(CultureInfo.InvariantCulture)}</color>");
	}
}