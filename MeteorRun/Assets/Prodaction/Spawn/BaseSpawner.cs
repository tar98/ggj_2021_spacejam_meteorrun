using UnityEngine;
using UnityEngine.Pool;
using Random = UnityEngine.Random;

public abstract class BaseSpawner<T> : MonoBehaviour where T : MonoBehaviour {
	[Header("Data")] 
	[SerializeField, Min(1)] protected int defaultCapacity = 20;
	[SerializeField, Min(50)] protected int maxSize = 500;
	[SerializeField] protected T[] prefabs;
	public ObjectPool<T> pool;

	protected T RandomPrefab => prefabs[Random.Range(0, prefabs.Length)];

	protected virtual void Start() {
		pool = new ObjectPool<T>(Create, Get, Release, Destroy, false, defaultCapacity, maxSize);
	}

	#region POOLING

	protected virtual T Create() {
		return Instantiate(RandomPrefab);
	}

	protected virtual void Get(T pooled) {
		pooled.gameObject.SetActive(true);
	}

	protected virtual void Release(T pooled) {
		pooled.gameObject.SetActive(false);
	}
	
	protected virtual void Destroy(T pooled) {
		Destroy(pooled.gameObject);
	}

	#endregion
}